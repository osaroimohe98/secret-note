QUICK START  
  
HOW TO RUN THIS PROJECT:  
1) Run `docker compose up` to build project  
  
or   
  
1) Create an env file using .env.example as a template 
2) Yarn install  
3) Yarn start:dev  


SERVER running on port `3000`

URLS:  

Get
1) /note    
    -- description: get all notes    
2) /note/:id     
     -- description: get an individual note        
     -- request params = `id` (identifier for individual note)       
     -- request body = `{ secretKey?: string }`     
     -- details: secretKey is optional, if the correct secretKey is passed with the request body the decrypted messaged is returned. If no secret key or an invalid key is passed, the encrypted message is returned.


Post  
1) /note  
   -- description: create a note      
   -- request body = `{ secretKey: string, message: string }`  
   -- details: all body params are required. secretKey length must greater than or equal to 16  