import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { NoteController } from './note.controller';
import { Note } from './note.entity';
import { NoteService } from './note.service';

@Module({
  exports: [TypeOrmModule],
  providers: [NoteService],
  controllers: [NoteController],
  imports: [TypeOrmModule.forFeature([Note])],
})
export class NoteModule {}
