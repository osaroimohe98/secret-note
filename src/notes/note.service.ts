import * as CryptoJS from 'crypto-js';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Note } from './note.entity';

@Injectable()
export class NoteService {
  constructor(
    @InjectRepository(Note)
    private noteRepository: Repository<Note>,
  ) {}
  async createNote(message: string, secretKey: string): Promise<Note> {
    try {
      const hashedMessage = CryptoJS.AES.encrypt(message, secretKey).toString();
      const newNote = new Note();
      newNote.message = hashedMessage;
      const note = await this.noteRepository.save(newNote);
      return note;
    } catch (e) {
      return;
    }
  }
  async findAll(): Promise<Note[]> {
    try {
      return this.noteRepository.find();
    } catch (e) {
      return;
    }
  }
  async findOne(id: number, secretKey: string): Promise<Note> {
    try {
      const note = await this.noteRepository.findOne({ where: { id } });
      if (secretKey && note) {
        const bytes = CryptoJS.AES.decrypt(note.message, secretKey);
        const m = bytes.toString(CryptoJS.enc.Utf8);
        if (m !== '') note.message = m;
      }
      return note;
    } catch (e) {
      return;
    }
  }
}
