import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { Note } from './note.entity';
import { NoteService } from './note.service';
import { CreateNoteDto, GetNoteDto } from './dto/note.dto';

@Controller('/note')
export class NoteController {
  constructor(private readonly noteService: NoteService) {}

  @Get('/test')
  test(): string {
    return `It's working`;
  }

  @Get('/:id')
  async getNote(
    @Body() body: GetNoteDto,
    @Param('id') id?: number,
  ): Promise<Note> {
    return await this.noteService.findOne(id, body.secretKey);
  }

  @Get()
  async getNotes(): Promise<Note[]> {
    return await this.noteService.findAll();
  }

  @Post()
  async newNote(@Body() body: CreateNoteDto): Promise<Note> {
    return await this.noteService.createNote(body.message, body.secretKey);
  }
}
