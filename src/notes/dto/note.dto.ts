import { IsNotEmpty, IsOptional, IsString, MinLength } from 'class-validator';

export class GetNoteDto {
  @IsOptional()
  @IsString()
  @MinLength(16)
  secretKey?: string;
}

export class CreateNoteDto {
  @IsNotEmpty()
  @IsString()
  message: string;

  @IsString()
  @MinLength(16)
  secretKey: string;
}
