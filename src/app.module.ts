import { Module } from '@nestjs/common';
import { NoteModule } from './notes/note.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './notes/note.entity';
import { ConfigModule, ConfigService } from '@nestjs/config';
import config from './config/config';

@Module({
  imports: [
    NoteModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('PG_HOST') || config.db.host,
        port: configService.get('PG_PORT') || config.db.port,
        username: configService.get('PG_USERNAME') || config.db.username,
        password: configService.get('PG_PASSWORD') || config.db.password,
        database: configService.get('PG_DB_NAME') || config.db.database,
        entities: [Note],
        synchronize: true,
      }),
    }),
  ],
})
export class AppModule {}
