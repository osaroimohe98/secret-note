type AppConfig = {
  port: number;
};

type Config = {
  app: AppConfig;
  db: {
    type: string;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
  };
};

// default config variables
const config: Config = {
  app: {
    port: 3000,
  },
  db: {
    port: 5432,
    type: 'postgres',
    host: 'localhost',
    username: 'postgres',
    password: 'postgres',
    database: 'test-db',
  },
};

export default config;
